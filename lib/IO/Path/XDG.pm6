#! /usr/bin/env false

use v6.d;

unit module IO::Path::XDG;

#| Returns an IO::Path for $XDG_CONFIG_HOME, if it exists as environment
#| variable. Otherwise, return the default value, $HOME/.config. This directory
#| should contain user-specific configuration files.
sub xdg-config-home (
	--> IO::Path
) is export {
	return %*ENV<XDG_CONFIG_HOME>.IO if %*ENV<XDG_CONFIG_HOME>:exists;

	$*HOME.add(".config");
}

#| Returns an IO::Path for $XDG_CACHE_HOME, if it exists as environment
#| variable. Otherwise, return the default value, $HOME/.cache. This directory
#| should contain user-specific, non-essential (cached) data.
sub xdg-cache-home (
	--> IO::Path
) is export {
	return %*ENV<XDG_CACHE_HOME>.IO if %*ENV<XDG_CACHE_HOME>:exists;

	$*HOME.IO.add(".cache");
}

#| Returns an IO::Path for $XDG_DATA_HOME, if it exists as environment
#| variable. Otherwise, return the default value, $HOME/.local/share. This
#| directory should contain user-specific data files.
sub xdg-data-home (
	--> IO::Path
) is export {
	return %*ENV<XDG_DATA_HOME>.IO if %*ENV<XDG_DATA_HOME>:exists;

	$*HOME.add(".local/share");
}

#| Returns an IO::Path for $XDG_RUNTIME_DIR, if it exists as environment
#| variable. Otherwise, return an IO::Path to a temporary directory. This
#| directory should contain user-specific runtime files and other file objects.
sub xdg-runtime-dir (
	--> IO::Path
) is export {
	return %*ENV<XDG_RUNTIME_DIR>.IO if %*ENV<XDG_RUNTIME_DIR>:exists;

	# XDG_RUNTIME_DIR is the only XDG basedir variant that does not come with
	# defaults. However, there are a number of de facto standard locations to
	# make use of. Try them, and return whichever hits first that also seems to
	# exist on the user's system.
	[
		"/var/run".IO.add(+$*USER),
		"/var/run".IO.add(~$*USER),
		$*HOME.add(".local/run"),
		$*TMPDIR,
		$*CWD,
	]
	.grep(*.d)
	.first
	.add($*PID)
}

=begin pod

=NAME    IO::Path::XDG
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
